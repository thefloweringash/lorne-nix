{ config, pkgs, lib, ... }:
let
  cfg = config.lorne-nix.emacs;

  fix-emacs = self: super: {
    # GTK means daemon is pretty much useless:
    # http://bugzilla.gnome.org/show_bug.cgi?id=85715
    emacs = super.emacs.override { withGTK3 = false; };
  };

  emacsWithPackages = (pkgs.emacsPackagesFor pkgs.emacs).emacsWithPackages;
in

{
  options = {
    lorne-nix.emacs.enable = lib.mkEnableOption "lorne-nix: emacs module";
  };

  config = lib.mkIf cfg.enable {
    nixpkgs.overlays = [ fix-emacs ];

    environment.systemPackages = [(
      emacsWithPackages (epkgs:
        (with epkgs.melpaStablePackages; [
          ag
          projectile
          magit
          nix-mode
          flycheck
        ]) ++ (with epkgs.melpaPackages; [
          evil
        ])
      )
    )];
  };
}

{ config, lib, pkgs, ... }:

with lib;

let
  cfg = config.lorne-nix.ssh-whitelist;

  authorizeCommands = { user, key, commands, ... }:
    let
      whitelistFile = pkgs.writeText "${user}-ssh-whitelist.yaml" (
        "---\ncommands:\n" + (concatMapStrings (command: "- " + command + "\n") commands)
      );
    in

    {
      ${user}.openssh.authorizedKeys.keys = [''
        restrict,command="${pkgs.ssh-whitelist}/bin/ssh-whitelist ${whitelistFile}" ${key}
      ''];
    };
in

{
  options = {
    lorne-nix.ssh-whitelist = {
      whitelist = mkOption {
        type = with types; listOf (submodule {
          options = {
            user     = mkOption { type = str; };
            key      = mkOption { type = str; };
            commands = mkOption { type = listOf str; };
          };
        });
        default = [];
      };

    };
  };

  config = mkIf (cfg.whitelist != []) {
    users.users = mkMerge (map authorizeCommands cfg.whitelist);
  };
}

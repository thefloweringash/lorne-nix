{ config, lib, pkgs, ... }:

let
  cfg = config.lorne-nix.private-vhosts;
in
{
  options = {
    lorne-nix.private-vhosts = {
      enable = lib.mkEnableOption "Private Vhosts";
    };
  };

  config = lib.mkIf cfg.enable {
    services.nginx.package = pkgs.nginxStable.override {
      pcre = pkgs.pcre;
      modules = with pkgs.nginxModules; [ lua ];
    };

    services.nginx.commonHttpConfig =
      let
        # This is definitely not sound.
        lua = pkgs.luajit;
        luaPackages = with pkgs; with lua.pkgs; [
          lua-resty-ipmatcher
          lua-resty-core
          lua-resty-lrucache
        ];

        mkLuaPackagePath = luaDeps: lib.concatStringsSep ";"
          (lib.concatMap (drv: [
            "${drv}/lib/lua/${lua.luaversion}/?.lua"
            "${drv}/share/lua/${lua.luaversion}/?.lua"
          ]) luaDeps);
      in
        ''
          lua_package_path '${mkLuaPackagePath luaPackages};?.lua';
        '';
  };
}

{ config, pkgs, lib, ... }:

with lib;

let
  cfg = config.services.sensu-go.agent;

  agentConfig = {
    name = cfg.name;
    "backend-url" = cfg.backendUrls;
    subscriptions = cfg.subscriptions;
  } // cfg.extraConfig;

  configJSON = pkgs.writeText "sensu-go-agent.json" (builtins.toJSON agentConfig);
  configYAML = pkgs.runCommand "sensu-go-agent.yaml" { preferLocalBuild = true; } ''
    ${pkgs.remarshal}/bin/json2yaml -i ${configJSON} -o $out
  '';
in
{
  options = {
    services.sensu-go.agent = {
      enable = mkEnableOption "Sensu Go Agent";

      name = mkOption {
        type = types.str;
        default = config.networking.hostName;
        description = ''
          Entity name assigned to the agent entity
        '';
      };

      backendUrls = mkOption {
        type = types.listOf types.str;
        default = [ "ws://127.0.0.1:8081" ];
        description = ''
          ws or wss URL of the Sensu backend server
        '';
        example = literalExpression ''
          [ "wss://sensu.example.org" ]
        '';
      };

      subscriptions = mkOption {
        type = types.listOf types.str;
        default = [];
        description = ''
          An array of agent subscriptions which determine which
          monitoring checks are executed by the agent.
        '';
        example = literalExpression ''
          [ "nixos" ]
        '';
      };

      packages = mkOption {
        type = types.listOf types.package;
        default = [];
        description = ''
          Packages implementing checks referenced from check definitions.
        '';
        example = literalExpression ''
          with pkgs; [ nagiosPluginsOfficial ];
        '';
      };

      environmentFile = mkOption {
        type = types.nullOr types.path;
        default = null;
        description = ''
          Path to a file containing environment variables. These will
          be set in the agent process, and can be used to inject
          secrets.
        '';
        example = "/etc/nixos/sensu-agent-secrets.env";
      };

      extraConfig = mkOption {
        type = types.attrs;
        default = {};
        description = "Extra configuration";
        example = literalExpression ''
          {
            labels = {
              os = "NixOS";
            };
            annotations = {
              "example-key" = "Example Value";
            };
        '';
      };

      extraFlags = mkOption {
        type = types.listOf types.str;
        default = [];
        description = "Extra command line flags";
        example = literalExpression ''
          [ "--api-port" "4000" ]
        '';
      };
    };
  };

  config = mkIf cfg.enable {
    systemd.services.sensu-go-agent = {
      description = "Sensu Go Agent";
      wantedBy = [ "multi-user.target" ];
      after = [ "network.target" ];

      # Sensu runs all checks via "sh -c ...", so a "sh" must be on
      # the path.
      path = [ pkgs.bash ] ++ cfg.packages;

      serviceConfig = {
        ExecStart = "${pkgs.sensu-go-agent}/bin/sensu-agent start --config-file ${configYAML} ${escapeShellArgs cfg.extraFlags}";
        DynamicUser = true;
        StateDirectory = "sensu/sensu-agent";
        CacheDirectory = "sensu/sensu-agent";
      } // optionalAttrs (cfg.environmentFile != null) {
        EnvironmentFile = cfg.environmentFile;
      };
    };
  };
}

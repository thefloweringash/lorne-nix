{ config, pkgs, lib, ... }:

with lib;

let
  cfg = config.services.sensu-go.backend;

  backendConfig = cfg.extraConfig;

  configJSON = pkgs.writeText "sensu-go-backend.json" (builtins.toJSON backendConfig);
  configYAML = pkgs.runCommand "sensu-go-backend.yaml" { preferLocalBuild = true; } ''
    ${pkgs.remarshal}/bin/json2yaml -i ${configJSON} -o $out
  '';
in
{
  options = {
    services.sensu-go.backend = {
      enable = mkEnableOption "Sensu Go Backend";

      extraConfig = mkOption {
        type = types.attrs;
        default = {};
        description = "Extra configuration";
        example = literalExpression ''
          { debug = true; }
        '';
      };

      extraFlags = mkOption {
        type = types.listOf types.str;
        default = [];
        example = literalExpression ''
          [ "--dashboard-port" "4000" ]
        '';
      };

      packages = mkOption {
        type = types.listOf types.package;
        default = [];
        description = ''
          Packages implementing handler commands referenced from handler definitions.
        '';
        example = literalExpression ''
          with pkgs; [ sensu-pagerduty-handler sensu-slack-handler ]
        '';
      };
    };
  };

  config = mkIf cfg.enable {
    systemd.services.sensu-go-backend = {
      description = "Sensu Go Backend";
      wantedBy = [ "multi-user.target" ];
      after = [ "network.target" ];

      # Sensu runs handlers via "sh -c ...", so a "sh" must be on the
      # path.
      path = [ pkgs.bash ] ++ cfg.packages;

      serviceConfig = {
        ExecStart = "${pkgs.sensu-go-backend}/bin/sensu-backend start --config-file ${configYAML} ${escapeShellArgs cfg.extraFlags}";
        DynamicUser = true;
        StateDirectory = "sensu/sensu-backend";
        CacheDirectory = "sensu/sensu-backend";
      };
    };
  };
}

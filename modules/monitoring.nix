{ config, lib, pkgs, ... }:

let
  cfg = config.lorne-nix.monitoring;
in

{
  options = {
    lorne-nix.monitoring.enable = lib.mkEnableOption "lorne-nix: monitoring module";
  };

  config = lib.mkIf cfg.enable {
    services.prometheus.exporters.node = {
      enable = true;
      enabledCollectors = [
        "systemd"
        "textfile"
      ];
      extraFlags = [
        "--collector.textfile.directory=/var/lib/prometheus-node-exporter-text-files"
      ];
    };

    # This is cute:
    # https://grahamc.com/blog/nixos-system-version-prometheus but
    # it's maybe wrong? It's reporting the current profile version at
    # activation time not the configuration being activated (which
    # doesn't inherently have a version). I added the system
    # configuration being activated, and a test if the kernel doesn't
    # match.

    system.activationScripts.node-exporter-system = ''
      mkdir -pm 0775 /var/lib/prometheus-node-exporter-text-files
      (
        cd /var/lib/prometheus-node-exporter-text-files
        (
          echo -n "system_version ";
          readlink /nix/var/nix/profiles/system | cut -d- -f2

          echo "system_config{config=\"$(basename $systemConfig)\"} 1"

          [ ! -e /run/booted-system/kernel ] || \
          [ "$(readlink /run/booted-system/kernel)" = "$(readlink $systemConfig/kernel)" ]
          echo "system_needs_reboot $?"
        ) > system.prom.next
        mv system.prom.next system.prom
      )
    '';

    services.sensu-go.agent = {
      enable = true;
      subscriptions = [ "nixos" ];
      environmentFile = "/etc/nixos/sensu.env";
      extraConfig = {
        backend-url = "wss://agent.sensu.cons.org.nz:443";
        annotations = {
          "nixos-label" = config.system.nixos.label;
        };
      };
      packages = with pkgs; [ nagiosPluginsOfficial ];
    };
  };
}

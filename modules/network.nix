{ config, lib, pkgs, ... }:

let
  cfg = config.lorne-nix.network;
in

{
  options = {
    lorne-nix.network.enable = lib.mkEnableOption "lorne-nix: network module";
  };

  config = lib.mkMerge [
    (lib.mkIf cfg.enable {
      environment.systemPackages = with pkgs; [
        networkmanagerapplet
      ];

      networking.networkmanager = {
        enable = true;

        extraConfig = ''
          [main]
          rc-manager = resolvconf
        '';

        # This should be sufficient to make NetworkManager always add
        # search domains. However, it doesn't work. (TODO: why?).

        # extraConfig = ''
        #   [global-dns]
        #   enable=yes
        #   searches=cons.org.nz
        # '';
      };

      # NetworkManager participates in resolvconf, so we can add the
      # search domains there instead.
      networking.resolvconf.extraConfig = ''
        search_domains='cons.org.nz'
      '';

      # Prevent dhcpcd and NetworkManager from fighting
      networking.dhcpcd.enable = false;
    })
  ];
}

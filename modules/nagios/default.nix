{ config, options, pkgs, lib, ... }:

let
  cfg = config.lorne-nix.nagios;

  phpSocket = config.services.phpfpm.pools.nagios.socket;
  cgiSocket = "/run/nagios-cgi.sock";
  nginx = config.services.nginx.package;
  nagios = pkgs.nagios;

  includeDir = path:
    let
      isCfg = name: (builtins.match ".*\.cfg" name) != null;
      # copy to store
      storePath = builtins.path {
        name = "objects";
        inherit path;
        filter = name: type: isCfg name;
      };
      contents = lib.filterAttrs (name: type: isCfg name) (builtins.readDir path);
    in lib.mapAttrsToList (name: type: "${storePath}/${name}") contents;

  pushoverScript = pkgs.callPackage ({ runCommandNoCC, lib, curl, makeWrapper }:
    runCommandNoCC "notify-pushover" {
      nativeBuildInputs = [ makeWrapper ];
    } ''
      mkdir -p $out/bin
      cp ${./notify_by_pushover.sh} $out/bin/pushover.sh
      chmod a+x $out/bin/pushover.sh
      wrapProgram $out/bin/pushover.sh --prefix PATH : ${lib.makeBinPath [ curl ]}
      patchShebangs $out
    ''
  ) {};

  checkMumble = pkgs.callPackage ({ runCommandNoCC, python3 }:
    runCommandNoCC "check_mumble" {
      buildInputs = [ python3 ];
    } ''
      mkdir -p $out/bin
      cp ${./check_mumble.py} $out/bin/check_mumble
      chmod a+x $out/bin/check_mumble
      patchShebangs $out
    ''
  ) {};

  contactType = with lib; types.submodule {
    options = {
      use = mkOption {
        type = types.str;
        default = "generic-contact";
      };
      alias = mkOption {
        type = types.str;
      };
      email = mkOption {
        type = types.str;
      };
      key = mkOption {
        type = types.str;
      };
      extraConfig = mkOption {
        type = types.str;
        default = "";
      };
    };
  };
in
{
  options = with lib; {
    lorne-nix.nagios = {
      enable = mkEnableOption "lorne's nagios config";

      nginx = {
        vhost = mkOption {
          type = types.str;
          default = "default";
        };

        extraConfig = mkOption {
          type = types.str;
          default = "";
        };

        cgiExtraConfig = mkOption {
          type = types.str;
          default = "";
        };
      };

      admins = mkOption {
        type = types.listOf types.str;
        default = [];
      };

      objectsDir = mkOption {
        type = types.listOf types.path;
        default = [];
      };

      pushover = {
        applicationKey = mkOption {
          type = types.str;
        };
        contacts = mkOption {
          type = types.attrsOf contactType;
        };
      };
    };
  };

  config = lib.mkIf cfg.enable (lib.mkMerge [
    {
      services.nginx = {
        enable = true;
        virtualHosts."${cfg.nginx.vhost}".locations."/nagios/" = {
          alias = "${nagios}/share/";
          index = "index.php";
          extraConfig = ''
            ${cfg.nginx.extraConfig}
            location ~ \.php$ {
              fastcgi_pass unix:${phpSocket};
              fastcgi_param SCRIPT_FILENAME $request_filename;
              include ${nginx}/conf/fastcgi_params;
            }

            location /nagios/cgi-bin/ {
              alias ${nagios}/bin/;
              fastcgi_pass unix:${cgiSocket};
              fastcgi_param NAGIOS_CGI_CONFIG ${config.services.nagios.cgiConfigFile};
              fastcgi_param SCRIPT_FILENAME $request_filename;
              ${cfg.nginx.cgiExtraConfig}
              include ${nginx}/conf/fastcgi_params;
            }
          '';
        };
      };

      services.phpfpm.pools.nagios = {
        user = "nagios";
        settings = {
          "pm" = "ondemand";
          "pm.max_children" = 8;

          "listen.owner" = "nginx";
          "listen.group" = "nginx";
          "listen.mode" = "0600";
        };
      };

      systemd.services.fcgiwrap-nagios = {
        after = [ "nss-user-lookup.target" ];
        serviceConfig = {
          ExecStart = "${pkgs.fcgiwrap}/bin/fcgiwrap";
          User = "nagios";
        };
      };

      systemd.sockets.fcgiwrap-nagios = {
        wantedBy = [ "sockets.target" ];
        socketConfig = {
          ListenStream = cgiSocket;
          SocketMode = "0600";
          SocketUser = "nginx";
          SocketGroup = "nginx";
        };
      };

      services.nagios.enable = true;
      services.nagios.cgiConfigFile = pkgs.writeText "nagios.cgi.conf" ''
        main_config_file=/etc/nagios.cfg
        url_html_path=/nagios
        use_authentication=1
        authorized_contactgroup_for_system_information=admins
        authorized_contactgroup_for_configuration_information=admins
        authorized_contactgroup_for_system_commands=admins
        authorized_contactgroup_for_all_services=admins
        authorized_contactgroup_for_all_hosts=admins
        authorized_contactgroup_for_all_service_commands=admins
        authorized_contactgroup_for_all_host_commands=admins
      '';
      services.nagios.plugins = options.services.nagios.plugins.default ++ [ checkMumble ];
      services.nagios.objectDefs =
        includeDir ./shared-objects ++ (lib.concatMap (dir: includeDir dir) cfg.objectsDir);
    }

    {
      services.nagios.objectDefs = [
        "${pkgs.writeText "admins.cfg" ''
          define contactgroup {
              contactgroup_name   admins
              alias               Nagios Administrators
              members             ${lib.concatStringsSep " " cfg.admins}
          }
        ''}"
      ];
    }

    (lib.mkIf (cfg.pushover.contacts != {}) {
      services.nagios.plugins = [ pushoverScript ];
      services.nagios.objectDefs = [
        "${pkgs.writeText "pushover.cfg" (''
          define command {
              command_name        notify-host-by-pushover
              command_line        pushover.sh -u '$CONTACTADDRESS1$' -a '${cfg.pushover.applicationKey}' -t '$NOTIFICATIONTYPE$ $HOSTNAME$ is $HOSTSTATE$' -m '$HOSTOUTPUT$' -p 1 -s spacealarm
          }

          define command {
              command_name        notify-service-by-pushover
              command_line        pushover.sh -u '$CONTACTADDRESS1$' -a '${cfg.pushover.applicationKey}' -t '$NOTIFICATIONTYPE$ $HOSTALIAS$/$SERVICEDESC$ is $SERVICESTATE$' -m '$SERVICEOUTPUT$' -p 1 -s spacealarm
          }
        '' + lib.concatStrings (lib.flip lib.mapAttrsToList cfg.pushover.contacts (name: contactCfg: ''
          define contact {
              contact_name        ${name}
              use                 ${contactCfg.use}
              alias               ${contactCfg.alias}

              email               ${contactCfg.email}
              address1            ${contactCfg.key}

              host_notification_commands notify-host-by-pushover
              service_notification_commands notify-service-by-pushover

              ${contactCfg.extraConfig}
          }
        '')))}"
      ];
    })
  ]);
}

#!/usr/bin/env python
# -*- coding: utf-8

from struct import *
import socket, sys, time, datetime
import argparse

FIELD_NAMES=["version", "timestamp", "users", "max_users", "bandwidth_limit"]
def query_server(host, port):
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s.settimeout(1)

        buf = pack(">iQ", 0, datetime.datetime.now().microsecond)
        s.sendto(buf, (host, port))

        data, addr = s.recvfrom(1024)

        r = dict(zip(FIELD_NAMES, unpack(">iQiii", data)))

        version = (r['version'] >> 16, r['version'] >> 8 & 0xFF, r['version'] & 0xFF)
        r['version_tuple'] = version

        ping = (datetime.datetime.now().microsecond - r['timestamp']) / 1000.0
        if ping < 0:
                ping = ping + 1000
        r['ping'] = ping

        return r

parser = argparse.ArgumentParser(prog=sys.argv[0])
parser.add_argument('-H', '--host')
parser.add_argument('-p', '--port', default=64738, type=int)
args = parser.parse_args()

try:
        rs = query_server(args.host, args.port)
        print "Mumble OK - Users = %(users)s, Check time = %(ping)sms" % rs
        sys.exit(0)
except Exception as e:
        print "CRITICAL - %s" % e
        sys.exit(2)







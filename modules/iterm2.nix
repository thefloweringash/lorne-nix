{ config, lib, pkgs, ... }:

let
  cfg = config.lorne-nix.programs.iterm2;

  inherit (lib) mkEnableOption mkIf;
in
{
  options = {
    lorne-nix.programs.iterm2.enable = mkEnableOption "lorne-nix: iterm2";
  };

  config = mkIf cfg.enable {
    environment.systemPackages = [ pkgs.iterm2-shell-integration ];

    programs = {
      bash.interactiveShellInit = ''
        source "${pkgs.iterm2-shell-integration}/share/iterm2/shell_integration/bash"
      '';

      zsh.interactiveShellInit = ''
        source "${pkgs.iterm2-shell-integration}/share/iterm2/shell_integration/zsh"
      '';

      fish.interactiveShellInit = ''
        source "${pkgs.iterm2-shell-integration}/share/iterm2/shell_integration/fish"
      '';
    };
  };
}

let
  withConfig = conf: import <nixpkgs/nixos/lib/eval-config.nix> {
    modules = [ ./default.nix conf ];
  };

  pkgsWithConfig = conf: (withConfig conf).pkgs;

  pkgs = pkgsWithConfig {};
in
{
  inherit (pkgs)
    sensu-go-pushover-handler
    iterm2-shell-integration
    lua-resty-ipmatcher
    lua-resty-core
    lua-resty-lrucache
    ;

  inherit (pkgsWithConfig { lorne-nix.emacs.enable = true; })
    emacs;
}

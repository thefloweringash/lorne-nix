{ config, lib, pkgs, ... }:

{
  imports = [
    ./modules/network.nix
    ./modules/monitoring.nix
    ./modules/ssh-whitelist.nix
    ./modules/private-vhosts.nix
    ./modules/nagios/default.nix
    ./modules/sensu-go/agent.nix
    ./modules/sensu-go/backend.nix
  ];
}

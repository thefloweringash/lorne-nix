{ config, lib, pkgs, ... }:

{
  imports = [
    ./modules/emacs.nix
    ./modules/iterm2.nix
  ];

  nixpkgs.overlays = [ (import ./packages/overlay.nix) ];
}

{ config, lib, pkgs, ... }:

{
  imports = [
    ./common-modules.nix
    ./nixos-modules.nix
  ];
}

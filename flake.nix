{
  description = "lorne's nix modules";

  outputs = { self }: {
    nixosModule = {
      imports = [
        ./common-modules.nix
        ./nixos-modules.nix
      ];
    };
    darwinModule = {
      imports = [
        ./common-modules.nix
      ];
    };
  };
}

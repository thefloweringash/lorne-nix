{ buildGoModule, fetchFromGitHub, lib }:

buildGoModule rec {
  pname = "sensu-go-pushover-handler";
  version = "1.1.0";

  src = fetchFromGitHub {
    owner = "nixwiz";
    repo = pname;
    rev = "d81956b01986577570dfea7dfa5ea7884ae6aae6";
    sha256 = "0dg2jp7vcpqn2ks3b5wcn17l794j8agli9q2rvhmydwm6k6gq1gq";
  };

  vendorHash = "sha256:03l95bnxz94wqn4bqrvbbdg4xbkig969izr23n0a4nbha5lh44gi";

  meta = {
    homepage = https://github.com/nixwiz/sensu-go-pushover-handler;
    description = "Sensu Go Pushover Handler Plugin";
    license = lib.licenses.mit;
  };
}

{ stdenv, lib, python3, fetchFromGitHub }:

stdenv.mkDerivation {
  pname = "iterm2-shell-integration";
  version = "2022-06-30";

  # Fetch a subdirectory
  src = fetchFromGitHub {
    owner = "gnachman";
    repo = "iTerm2-shell-integration";
    rev = "67e9313f992f824f5102491fcb587611ba948035";
    sha256 = "sha256-L7e3yNlrzk/sImu8wHl3Sc5TQg/kVQlY2/VBBUUuYww";
  };

  buildInputs = [ python3 ];

  dontBuild = true;

  installPhase = ''
    rm -vr shell_integration/{*.sh,Makefile,source}

    mkdir -p $out/{bin,share/iterm2}
    cp -r utilities/* $out/bin
    cp -r shell_integration $out/share/iterm2/
  '';
}

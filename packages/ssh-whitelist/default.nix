{ stdenv, ruby }:

stdenv.mkDerivation {
  name = "ssh-whitelist";
  src = ./ssh-whitelist;

  buildInputs = [ ruby ];

  unpackPhase = ''
    cp $src ./ssh-whitelist
  '';

  installPhase = ''
    mkdir -p $out/bin
    cp ssh-whitelist $out/bin
  '';
}

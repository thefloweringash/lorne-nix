self: super: {
  ssh-whitelist = super.callPackage ./ssh-whitelist {};

  sensu-pagerduty-handler = super.callPackage ./sensu-pagerduty-handler {};

  sensu-slack-handler = super.callPackage ./sensu-slack-handler {};

  sensu-go-pushover-handler = super.callPackage ./sensu-go-pushover-handler {};

  iterm2-shell-integration = super.callPackage ./iterm2-shell-integration {};

  # TODO: this probably isn't sound
  lua-resty-ipmatcher = super.luajit.pkgs.callPackage ./lua-resty-ipmatcher {};
}

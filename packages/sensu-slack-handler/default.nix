{ buildGoPackage, fetchFromGitHub, lib }:

buildGoPackage rec {
  name = "sensu-slack-handler";
  version = "1.0.3";

  goPackagePath = "github.com/sensu/${name}";

  src = fetchFromGitHub {
    owner = "sensu";
    repo = name;
    rev = "${version}";
    sha256 = "11ggml4qqcxq14saslnvzaq106hisr5nc4ixxshml1mrxx4kjisq";
  };

  goDeps = ./deps.nix;

  meta = {
    homepage = https://github.com/sensu/sensu-slack-handler;
    description = "The Sensu Go Slack handler for notifying a channel";
    license = lib.licenses.mit;
  };
}

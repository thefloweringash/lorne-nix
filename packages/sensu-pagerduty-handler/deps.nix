# file generated from Gopkg.lock using dep2nix (https://github.com/nixcloud/dep2nix)
[
  {
    goPackagePath  = "github.com/PagerDuty/go-pagerduty";
    fetch = {
      type = "git";
      url = "https://github.com/PagerDuty/go-pagerduty";
      rev =  "fe8f9c4593d05f029afeb2c08c3e220120306506";
      sha256 = "15kyfjjch8dw0mbb50hjlm952ry387qyf8jb48ajgkml29rny20g";
    };
  }
  {
    goPackagePath  = "github.com/coreos/etcd";
    fetch = {
      type = "git";
      url = "https://github.com/coreos/etcd";
      rev =  "27fc7e2296f506182f58ce846e48f36b34fe6842";
      sha256 = "1x2ii1hj8jraba8rbxz6dmc03y3sjxdnzipdvg6fywnlq1f3l3wl";
    };
  }
  {
    goPackagePath  = "github.com/dgrijalva/jwt-go";
    fetch = {
      type = "git";
      url = "https://github.com/dgrijalva/jwt-go";
      rev =  "06ea1031745cb8b3dab3f6a236daf2b0aa468b7e";
      sha256 = "08m27vlms74pfy5z79w67f9lk9zkx6a9jd68k3c4msxy75ry36mp";
    };
  }
  {
    goPackagePath  = "github.com/echlebek/timeproxy";
    fetch = {
      type = "git";
      url = "https://github.com/echlebek/timeproxy";
      rev =  "d7c2918981322d4d437f7a52f3a15c7ec5a4f488";
      sha256 = "1wjdb6ianzvg81krhhg4cjnc6mg35z6j8c4rgk9ygkjsdd5lsqgv";
    };
  }
  {
    goPackagePath  = "github.com/gogo/protobuf";
    fetch = {
      type = "git";
      url = "https://github.com/gogo/protobuf";
      rev =  "636bf0302bc95575d69441b25a2603156ffdddf1";
      sha256 = "1525pq7r6h3s8dncvq8gxi893p2nq8dxpzvq0nfl5b4p6mq0v1c2";
    };
  }
  {
    goPackagePath  = "github.com/golang/protobuf";
    fetch = {
      type = "git";
      url = "https://github.com/golang/protobuf";
      rev =  "aa810b61a9c79d51363740d207bb46cf8e620ed5";
      sha256 = "0kf4b59rcbb1cchfny2dm9jyznp8ri2hsb14n8iak1q8986xa0ab";
    };
  }
  {
    goPackagePath  = "github.com/google/go-querystring";
    fetch = {
      type = "git";
      url = "https://github.com/google/go-querystring";
      rev =  "44c6ddd0a2342c386950e880b658017258da92fc";
      sha256 = "0xl12bqyvmn4xcnf8p9ksj9rmnr7s40pvppsdmy8n9bzw1db0iwz";
    };
  }
  {
    goPackagePath  = "github.com/inconshreveable/mousetrap";
    fetch = {
      type = "git";
      url = "https://github.com/inconshreveable/mousetrap";
      rev =  "76626ae9c91c4f2a10f34cad8ce83ea42c93bb75";
      sha256 = "1mn0kg48xkd74brf48qf5hzp0bc6g8cf5a77w895rl3qnlpfw152";
    };
  }
  {
    goPackagePath  = "github.com/json-iterator/go";
    fetch = {
      type = "git";
      url = "https://github.com/json-iterator/go";
      rev =  "f7279a603edee96fe7764d3de9c6ff8cf9970994";
      sha256 = "096vy937cbhrv0wzlb18clm8h6hpgmca5pvhkqvyvccq6rzpwx7z";
    };
  }
  {
    goPackagePath  = "github.com/robertkrimen/otto";
    fetch = {
      type = "git";
      url = "https://github.com/robertkrimen/otto";
      rev =  "15f95af6e78dcd2030d8195a138bd88d4f403546";
      sha256 = "07j7l340lmqwpfscwyb8llk3k37flvs20a4a8vzc85f16xyd9npf";
    };
  }
  {
    goPackagePath  = "github.com/robfig/cron";
    fetch = {
      type = "git";
      url = "https://github.com/robfig/cron";
      rev =  "2315d5715e36303a941d907f038da7f7c44c773b";
      sha256 = "0s29yv5sl9iabcpfvnq1bsd7zvwxkzrmq650x06cnlird9av1bv8";
    };
  }
  {
    goPackagePath  = "github.com/sensu/sensu-go";
    fetch = {
      type = "git";
      url = "https://github.com/sensu/sensu-go";
      rev =  "2daf9d442deec0afd2c6f53c183460e879a10646";
      sha256 = "1ai2inpyx0222qxri4fd9phh3cs5hkpwi347imkza231k8g7p6wk";
    };
  }
  {
    goPackagePath  = "github.com/spf13/cobra";
    fetch = {
      type = "git";
      url = "https://github.com/spf13/cobra";
      rev =  "ef82de70bb3f60c65fb8eebacbb2d122ef517385";
      sha256 = "1q1nsx05svyv9fv3fy6xv6gs9ffimkyzsfm49flvl3wnvf1ncrkd";
    };
  }
  {
    goPackagePath  = "github.com/spf13/pflag";
    fetch = {
      type = "git";
      url = "https://github.com/spf13/pflag";
      rev =  "298182f68c66c05229eb03ac171abe6e309ee79a";
      sha256 = "1cj3cjm7d3zk0mf1xdybh0jywkbbw7a6yr3y22x9sis31scprswd";
    };
  }
  {
    goPackagePath  = "golang.org/x/net";
    fetch = {
      type = "git";
      url = "https://go.googlesource.com/net";
      rev =  "351d144fa1fc0bd934e2408202be0c29f25e35a0";
      sha256 = "1c5x25qjyz83y92bq0lll5kmznyi3m02wd4c54scgf0866gy938k";
    };
  }
  {
    goPackagePath  = "golang.org/x/sys";
    fetch = {
      type = "git";
      url = "https://go.googlesource.com/sys";
      rev =  "a5c9d58dba9a56f97aaa86f55e638b718c5a6c42";
      sha256 = "02qv5i7yps35p7fa81345qz7k8i73gkigj69anwmpw9rhpmzayf9";
    };
  }
  {
    goPackagePath  = "golang.org/x/text";
    fetch = {
      type = "git";
      url = "https://go.googlesource.com/text";
      rev =  "f21a4dfb5e38f5895301dc265a8def02365cc3d0";
      sha256 = "0r6x6zjzhr8ksqlpiwm5gdd7s209kwk5p4lw54xjvz10cs3qlq19";
    };
  }
  {
    goPackagePath  = "google.golang.org/genproto";
    fetch = {
      type = "git";
      url = "https://github.com/google/go-genproto";
      rev =  "bd91e49a0898e27abb88c339b432fa53d7497ac0";
      sha256 = "1f5q04h03q6fksbfkhz13ai5849rkkb8xrmmi7cxs4lzsi6ixkg8";
    };
  }
  {
    goPackagePath  = "google.golang.org/grpc";
    fetch = {
      type = "git";
      url = "https://github.com/grpc/grpc-go";
      rev =  "df014850f6dee74ba2fc94874043a9f3f75fbfd8";
      sha256 = "0ibwav7p5cqng8yslarix521i1s11r7w9y2rjliahm75cj5crrb8";
    };
  }
  {
    goPackagePath  = "gopkg.in/sourcemap.v1";
    fetch = {
      type = "git";
      url = "https://github.com/go-sourcemap/sourcemap";
      rev =  "6e83acea0053641eff084973fee085f0c193c61a";
      sha256 = "08rf2dl13hbnm3fq2cm0nnsspy9fhf922ln23cz5463cv7h62as4";
    };
  }
]
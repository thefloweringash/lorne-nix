{ buildGoPackage, fetchFromGitHub, lib }:

buildGoPackage rec {
  name = "sensu-pagerduty-handler";
  version = "1.1.0";

  goPackagePath = "github.com/sensu/${name}";

  src = fetchFromGitHub {
    owner = "sensu";
    repo = name;
    rev = "${version}";
    sha256 = "1ygzvdzgl7dgzsbfnpixk7cg5wy8p5sijxiwmx2wj6jy088rk50v";
  };

  goDeps = ./deps.nix;

  meta = {
    homepage = https://github.com/sensu/sensu-pagerduty-handler;
    description = "Sensu Go PagerDuty Handler";
    license = lib.licenses.mit;
  };
}

{ buildLuarocksPackage, luaOlder, luaAtLeast
, fetchgit, fetchurl }:
buildLuarocksPackage {
  pname = "lua-resty-ipmatcher";
  version = "0.6.1-0";
  knownRockspec = (fetchurl {
    url    = "mirror://luarocks/lua-resty-ipmatcher-0.6.1-0.rockspec";
    sha256 = "1i2hshqfp0kwsbhcjpw051in31a5n9fybphf1ivmh9g2kiaxbjdm";
  }).outPath;
  src = fetchgit ( removeAttrs (builtins.fromJSON ''{
  "url": "https://github.com/api7/lua-resty-ipmatcher",
  "rev": "62d4c44d67227e8f3fe02331c2f8b90fe0d7ccd1",
  "date": "2021-08-09T09:29:11+08:00",
  "path": "/nix/store/pbhagf2cibnhxpffz198if9zk7jzvvwf-lua-resty-ipmatcher",
  "sha256": "044mhlwvfpr8m7hf7kh8ccna4dnifgavv8avcwxn5lzf3lqj4jsw",
  "fetchLFS": false,
  "fetchSubmodules": true,
  "deepClone": false,
  "leaveDotGit": false
}
 '') ["date" "path"]) ;


  meta = {
    homepage = "https://github.com/api7/lua-resty-ipmatcher";
    description = "High performance match IP address for Lua(OpenResty).";
    license.fullName = "Apache License 2.0";
  };
}

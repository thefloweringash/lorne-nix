{ pkgs, ... }:
{
  inherit (import ./private-vhosts { inherit pkgs; })
    privateVhost
    privateLocation
    privateLocationExtraConfig
    ;
}

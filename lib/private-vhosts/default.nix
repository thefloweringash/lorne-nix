{ pkgs, ... }:
let
  writeLuaModule = name: text:
    pkgs.writeTextFile rec {
      inherit name text;
      destination = "/${name}.lua";
      checkPhase = ''
        ${pkgs.lua}/bin/luac -p $out/$name.lua
      '';
    };
in
rec {
  privateVhost = attrs: attrs // {
    extraConfig = ''
      ssl_verify_client optional;
      ssl_verify_depth 5;
      ssl_client_certificate ${./cons-login-ca.crt};
    '' + (attrs.extraConfig or "");
  };

  privateLocationExtraConfig = {
    allowCNs ? [ "lorne@cons.org.nz" ],
    allowIPs ? [ ],
  }:
  let
    access_data = writeLuaModule "access_data" ''
      local _M = {}

      local ipmatcher = require("resty.ipmatcher")

      local permitted_cns = {
        ${pkgs.lib.concatMapStringsSep ",\n" (cn: "[\"${cn}\"] = true") allowCNs }
      }

      local allowed_ips, err = ipmatcher.new({
        ${pkgs.lib.concatMapStringsSep ",\n" (ip: "\"${ip}\"") allowIPs }
      })

      function _M.cn_permitted(cn)
        return permitted_cns[cn]
      end

      function _M.ip_permitted(bin_ip)
        local ok, err = allowed_ips:match_bin(bin_ip)
        return ok
      end

      return _M
    '';

    access_control = writeLuaModule "access_control" ''
      local access_data = require "${access_data}/access_data"

      local function parse_cn()
        return string.match(ngx.var.ssl_client_s_dn, ",CN=([^,]+)")
      end

      if access_data.ip_permitted(ngx.var.binary_remote_addr) then
        return
      end

      local ssl_client_verify = ngx.var.ssl_client_verify
      if ssl_client_verify == "NONE" then
        ngx.exit(401)
      elseif ssl_client_verify == "SUCCESS" then
        local cn = parse_cn()
        if cn and access_data.cn_permitted(cn) then
          ngx.var.ssl_client_s_dn_cn = cn
          return
        end
      end

      ngx.exit(403)
    '';
   in ''
    set $ssl_client_s_dn_cn "";
    access_by_lua_file ${access_control}/access_control.lua;
  '';

  privateLocation = cfg: attrs: attrs // {
    extraConfig = privateLocationExtraConfig cfg + (attrs.extraConfig or "");
  };
}
